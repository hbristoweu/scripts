#!/bin/bash
# BBC News article parser
# jdon, har

# $1	Article URL

# Try	less $(./bbc-carver http://bbc.com/article123)
#  for quick reading

datetime=$(date "+%Y.%m.%d-%H.%M.%S")
outfilename="$datetime""_report_bbc"

curl -s $1 | grep story-body > t_in

# Change \n to nothing for a leaner output
sed -e $"s/<[^>]\+>/\n/g" t_in > t_out

#head -n -6 t_out
head -n -6 t_out > $outfilename

rm t_*

#Remove this if you like
echo $outfilename
