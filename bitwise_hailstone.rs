
fn main() {

    let mut x: u32 = 11;

    while x != 1 {

        println!("{}",x);
        
        if x & 1 == 1 {

            // Odd

            let t: u32 = x;

            x <<= 1;
            x += 1 + t;

        } else {

            // Even

            x >>= 1;

        }

    }

    println!("{}",x);

}
