#!/bin/bash
# BBC News (Hardcoded for Politics) Latest Links Grabber
# jdon, har

# Collects the most recents links from BBC politics and dumps them to a file called links.txt

# $1	Number of links to present (Default 10)

wget http://www.bbc.co.uk/news/politics -O file

cat file | awk '/http/{print }' | grep -E *-[0-9]{8}/ |  sed -n -e 's/^.* "//p' | sed s/\"//g | sed 's/.*-\([0-9]*\)/\1 &/' | sort -unr | sed 's/\S* //' | head -n ${1:-10} > links.txt

rm file
