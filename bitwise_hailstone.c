#include <stdio.h>
#include <stdint.h>

int main (int argc, char* argv[]){
	
	uint32_t x = 7, t;
	
	while (x != 1){
		
		printf("%d,",x);
		
		if ( x & 1 ){
			
			/* Odd	*/
			
			t = x;
			
			x <<= 1;
			x += 1 + t;
			
		} else {
			
			/* Even	*/
			
			x >>= 1;
			
		}
	}
	
	printf("%d",x);
	
	return(0);
}
