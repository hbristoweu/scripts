# Copyright 2012 James McCauley
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This component is for use with the OpenFlow tutorial.

It acts as a simple hub, but can be modified to act like an L2
learning switch.

It's roughly similar to the one Brandon Heller did for NOX.
"""

from pox.core import core
import pox.openflow.libopenflow_01 as of

log = core.getLogger()



class Tutorial (object):
  """
  A Tutorial object is created for each switch that connects.
  A Connection object for that switch is passed to the __init__ function.
  """
  def __init__ (self, connection):
    # Keep track of the connection to the switch so that we can
    # send it messages!
    self.connection = connection

    # This binds our PacketIn event listener
    connection.addListeners(self)

    # Use this table to keep track of which ethernet address is on
    # which switch port (keys are MACs, values are ports).
    self.mac_to_port = {}


  def resend_packet (self, packet_in, out_port):
    """
    Instructs the switch to resend a packet that it had sent to us.
    "packet_in" is the ofp_packet_in object the switch had sent to the
    controller due to a table-miss.
    """
    msg = of.ofp_packet_out()
    msg.data = packet_in

    # Add an action to send to the specified port
    action = of.ofp_action_output(port = out_port)
    msg.actions.append(action)

    # Send message to switch
    self.connection.send(msg)


  def act_like_hub (self, packet, packet_in):
    """
    Implement hub-like behavior -- send all packets to all ports besides
    the input port.
    """

    # We want to output to all ports -- we do that using the special
    # OFPP_ALL port as the output port.  (We could have also used
    # OFPP_FLOOD.)
    self.resend_packet(packet_in, of.OFPP_ALL)

    # Note that if we didn't get a valid buffer_id, a slightly better
    # implementation would check that we got the full data before
    # sending it (len(packet_in.data) should be == packet_in.total_len)).


  def act_like_switch (self, packet, packet_in):
    """
    Implement switch-like behavior.
    """

    # DELETE THIS LINE TO START WORKING ON THIS (AND THE ONE BELOW!) #

    # Here's some psuedocode to start you off implementing a learning
    # switch.  You'll need to rewrite it as real Python code.

    # Learn the port for the source MAC
    if packet.src not in self.mac_to_port:
      log.debug("Learning MAC...")
      self.mac_to_port[packet.src] = packet_in.in_port

    if packet.dst in self.mac_to_port:
      # Send packet out the associated port
      #self.resend_packet(packet_in, self.mac_to_port[packet.dst])

      log.debug("Installing flow [Src_p:" + str(packet.src) + " Dst_p:" + str(packet.dst) + "]" )
      # Maybe the log statement should have source/destination/port?

      msg = of.ofp_flow_mod()	# Create the flow msg container for the Openflow Protocol

      # Populate the fields of the message with required information
      # Match rules define the logic of the flow like IF statements.
      # http://archive.openflow.org/doc/gui/org/openflow/protocol/Match.html
      msg.match = of.ofp_match.from_packet(packet) # IF some packet MATCHES this packet obj ...
      msg.in_port = packet_in.in_port	# Defines the source port for this flow
      msg.match.dl_src = packet.src	# Ethernet src address (packet.src can be replaced with None)
      msg.match.fl_dst = packet.dst	# No idea what this does, flow destination?

      msg.idle_timeout = 10	 	# Idle timeout value, switch will remove the flow in case of no traffic
      msg.hard_timeout = 30 		# Hard timeout is for removing a rule unconditionally
      #msg.buffer_id = event.ofp.buffer_id    # Use the incoming packet as the data for the packet out

      # Add an output action, and send -- similar to resend_packet()
      action = of.ofp_action_output(port = self.mac_to_port[packet.dst])
      msg.actions.append(action)

      # Send the OFP message we just formulated to this OF switch
      self.connection.send(msg)

    else:
      # Switch defaults to flood
      log.debug("MAC not known, flooding then learning ...")
      self.resend_packet(packet_in, of.OFPP_ALL)



  def _handle_PacketIn (self, event):
    """
    Handles packet in messages from the switch.
    """

    packet = event.parsed # This is the parsed packet data.
    if not packet.parsed:
      log.warning("Ignoring incomplete packet")
      return

    packet_in = event.ofp # The actual ofp_packet_in message.

    # Comment out the following line and uncomment the one after
    # when starting the exercise.
    #self.act_like_hub(packet, packet_in)
    self.act_like_switch(packet, packet_in)



def launch ():
  """
  Starts the component
  """
  def start_switch (event):
    log.debug("Controlling %s" % (event.connection,))
    Tutorial(event.connection)
  core.openflow.addListenerByName("ConnectionUp", start_switch)
